import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { SET_LOCATION } from '../../store/location.reducer';
@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})
export class TopBarComponent implements OnInit {
  cities: String[];
  loc: string;
  constructor(private store: Store<any>) {
    this.cities = ['Amsterdam', 'Amstelveen', 'Utrecht', 'Rotterdam', 'The Hague'];
    this.loc = '';
  }
  ngOnInit() {
    this.search(this.cities[0]);
  }
  search(location: any) {
    this.loc = location;
    if (!this.loc) {
      return;
    }
    this.store.dispatch({ type: SET_LOCATION, payload: this.loc });
  }
}